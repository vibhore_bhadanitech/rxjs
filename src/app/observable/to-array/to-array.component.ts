import { Component, OnInit } from '@angular/core';
import { from, interval, observable, Subscription,of} from 'rxjs';
import { toArray, take } from 'rxjs/operators';

@Component({
  selector: 'app-to-array',
  templateUrl: './to-array.component.html',
  styleUrls: ['./to-array.component.scss']
})
export class ToArrayComponent implements OnInit {
  sourceSub !: Subscription
  users : any  =[
    {name : 'Anup' , skills : 'angular'},
    {name : 'Shekher' , skills : 'HTML'},
    {name : 'Sharma' , skills : 'CSS'},
    {name : 'UxTrendz' , skills : 'js'},
  ]

  constructor() { }

  ngOnInit(): void {
//Ex 01

  const source =  interval(1000);
  this.sourceSub = source.pipe(
    take(5),
    toArray()
  ).subscribe(res=>{
    console.log(res);
    // if(res >= 8){
    //   this.sourceSub.unsubscribe()
    // }

    //Ex-02
    const source2  = from(this.users);
    source2.pipe(toArray()).subscribe(res=>{
      console.log(res)
    })
  })
// Ex 03
  const source3 = of('anup','shekher','sharma','uztrendz');
  source3.pipe(toArray()).subscribe(res=>{
    console.log(res)
  })

  }
}
