import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.scss']
})
export class CustomComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    //ex -01 manual
    const custObs = Observable.create((observer: any) => {
      setTimeout(() =>{
        observer.next('data 1')
      },1000),
      setTimeout(() =>{
        observer.next('data 2')
      },2000),
      setTimeout(() =>{
        observer.next('data 3');
        observer.error(new Error('limit exceed'));
      },3000)
    })
    custObs.subscribe((res: any)=>{
      console.log(res)
    })
  }

}
