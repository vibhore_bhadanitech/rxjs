import { Component, OnInit } from '@angular/core';
import { from, of } from 'rxjs';
import { DesignUtilityService } from 'src/app/appServices/design-utility.service';

@Component({
  selector: 'app-of-from',
  templateUrl: './of-from.component.html',
  styleUrls: ['./of-from.component.scss']
})
export class OfFromComponent implements OnInit {
  obsMsg !: any;

  constructor(private _designUtility:DesignUtilityService) { }

  ngOnInit(): void {
      //of example

      const obs1 = of('Anup','Shekher','Sharma');

      obs1.subscribe(res=>{
        this._designUtility.print(res,'elComponent')
        console.log(res);
      })

      const obs2: any = of({a:'Anup',b:'Shekher',c:'Sharma'});

      obs2.subscribe((res : any)=>{
        this.obsMsg=res;
        console.log('obsMsg=>',res);
      })

      // from example
      const obs3 = from(['UxTrendz','John','Alex']);

      obs3.subscribe(res=>{
        console.log(res);
        this._designUtility.print(res,'elComponent3')
      })
 
      // from promise
      const promise = new Promise(resolve=>{
        setTimeout(()=>{
          resolve('Promise Resolved')
        },3000)
      })
      const obs4 = from(promise);
      obs4.subscribe(res=>{
        console.log(res);
        this._designUtility.print(res,'elComponent4')
      })

      //from string

      const obs5 = from('welcome to uxTrendz');
      obs5.subscribe(res=>{
        console.log(res);
        this._designUtility.print(res,'elComponent5')
      })


  }

}