import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { filter, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  dataArr = [
    {id : 1, name: 'Xjksnx', gender : 'Male'},
    {id : 2, name: 'Yjkcn', gender : 'Female'},
    {id : 3, name: 'Aru', gender : 'Male'},
    {id : 4, name: 'Blxcwd', gender : 'Male'},
    {id : 5, name: 'Cceveorko', gender : 'Female'},
    {id : 6, name: 'Deop', gender : 'Male'},
    {id : 7, name: 'Ecc', gender : 'Female'},
    {id : 8, name: 'Fepkdop', gender : 'Male'},
    {id : 9, name: 'Gekpoi', gender : 'Female'},
    {id : 10, name: 'Hcmk', gender : 'Male'},
    {id : 11, name: 'Icccw', gender : 'Female'},
    {id : 12, name: 'Jiei', gender : 'Female'},
    {id : 13, name: 'Kmcmw', gender : 'Male'},
    {id : 14, name: 'Lwokwoko', gender : 'Female'},
    {id : 15, name: 'Meiojo', gender : 'Male'},
    {id : 16, name: 'Ncjn', gender : 'Female'},
    {id : 17, name: 'Ocw', gender : 'Male'},
    {id : 18, name: 'Pnjcejkn', gender : 'Female'},

  ]
  data : any;
  data2 : any;
  data3 :any;

  constructor() { }

  ngOnInit(): void {

    const source = from(this.dataArr )
//Ex 01 filter by length

source.pipe(filter(member => member.name.length > 6 ),toArray()).subscribe(res=>{
  console.log(res)
  this.data = res;
})

source.pipe(filter(member => member.gender == 'Female' ),toArray()).subscribe(res=>{
  console.log(res)
  this.data2 = res;
})
source.pipe(filter(member => member.id <= 6 ),toArray()).subscribe(res=>{
  console.log(res)
  this.data3 = res;
})


  }

}
