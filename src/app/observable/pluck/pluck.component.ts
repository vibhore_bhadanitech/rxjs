import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { map, toArray,pluck } from 'rxjs/operators';


@Component({
  selector: 'app-pluck',
  templateUrl: './pluck.component.html',
  styleUrls: ['./pluck.component.scss']
})
export class PluckComponent implements OnInit {

  constructor() { }
  user = [
    {
      name : 'abc',
      skills : 'Angular',
      job :{
        title : 'ui developer',
        exp : '10 yrs'
      }
    },
    {
      name : 'def',
      skills : 'Html',
      job :{
        title : 'ui developer',
        exp : '10 yrs'
      }

    },
    {
      name : 'pqr',
      skills : 'Java Script',
      job :{
        title : 'trainee developer',
        exp : '10 yrs'
      }

    },
    {
      name : 'xyz',
      skills : 'React',
      job :{
        title : 'ui developer',
        exp : '10 yrs'
      }

    },
    {
      name : 'uvw',
      skills : 'JSON',      
      job : null

    },

  ]
  data !: any;

  ngOnInit(): void {
    from(this.user).pipe(
    //  map(data => data.job.exp),
     //pluck('job','title'),
     toArray( )
    ).subscribe(res=>{
      console.log(res)
      this.data = res;
    })
  }

}
