import { Component, OnInit } from '@angular/core';
import { interval, Subscription, timer } from 'rxjs';
import { DesignUtilityService } from 'src/app/appServices/design-utility.service';

@Component({
  selector: 'app-interval',
  templateUrl: './interval.component.html',
  styleUrls: ['./interval.component.scss']
})
export class IntervalComponent implements OnInit {
  obsMsg!: string;
  videoSubscription !:Subscription;

  constructor(private _designUtility: DesignUtilityService) { }

  ngOnInit(): void {

    //const broadCastVideo = interval(1000);
    const broadCastVideo = timer(2000,1000);

    
    this.videoSubscription= broadCastVideo.subscribe(res=>{
      console.log(res)
      this.obsMsg='video '+ res;
      this._designUtility.print(this.obsMsg,'elContainer');
      this._designUtility.print(this.obsMsg,'elContainer2');
      this._designUtility.print(this.obsMsg,'elContainer3');


      if(res>=5){
        this.videoSubscription.unsubscribe();
      }
    })
    
  }

}
