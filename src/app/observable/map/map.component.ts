import { Component, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  sub1 !: Subscription;
  msg1 !: any;

  constructor() { }

  ngOnInit(): void {
//ex -01
const BroadcastVideo = interval(1000); 

this.sub1 = BroadcastVideo.pipe(
  map(data => 'Video '+data)
).subscribe(res =>{
  this.msg1 = res;
console.log(this.msg1);
})

setTimeout(() => {
  this.sub1.unsubscribe()
}, 10000);

  }

}
