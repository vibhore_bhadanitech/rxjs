import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promise',
  templateUrl: './promise.component.html',
  styleUrls: ['./promise.component.scss']
})
export class PromiseComponent implements OnInit {

  constructor() { }
  dellAvailable(){
    return false;
  };
  hpAvailable(){
    return false;
  };
  promiseVal: any;

  ngOnInit(): void {
    let buyLaptop = new Promise((resolve,reject)=>{
      if(this.dellAvailable()){
        return setTimeout(()=>{
          resolve('c is purchased')
        },3000)
      }  
      else if(this.hpAvailable()){
        return setTimeout(()=>{
          resolve('hp is purchased')
        },3000)
      } 
      else{
        return setTimeout(()=>{
          reject('laptop not available')
        },3000)
      }
    });
    buyLaptop.then(res=>{
      console.log('then code=>',res)
      this.promiseVal=res;
    }).catch(res=>{
      console.log('catch code=>',res)
      this.promiseVal=res;
    })
  }
}