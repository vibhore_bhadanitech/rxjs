import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DesignUtilityService {

  constructor() { }

  print(val:any,containerId:string){
    let element = document.createElement('li');
    element.innerText = val;
    document.getElementById(containerId)?.appendChild(element);
  }

}
